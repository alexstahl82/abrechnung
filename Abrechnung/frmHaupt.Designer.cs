﻿namespace Abrechnung
{
    partial class frmHaupt
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.mnuHaupt = new System.Windows.Forms.MenuStrip();
            this.tsmNeu = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmAbrechnung = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHaupt.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.Location = new System.Drawing.Point(12, 586);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(120, 33);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Beenden";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // mnuHaupt
            // 
            this.mnuHaupt.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmNeu,
            this.tsmAbrechnung});
            this.mnuHaupt.Location = new System.Drawing.Point(0, 0);
            this.mnuHaupt.Name = "mnuHaupt";
            this.mnuHaupt.Size = new System.Drawing.Size(799, 24);
            this.mnuHaupt.TabIndex = 2;
            this.mnuHaupt.Text = "menuStrip1";
            // 
            // tsmNeu
            // 
            this.tsmNeu.Name = "tsmNeu";
            this.tsmNeu.Size = new System.Drawing.Size(92, 20);
            this.tsmNeu.Text = "Neues Projekt";
            this.tsmNeu.Click += new System.EventHandler(this.tsmNeu_Click);
            // 
            // tsmAbrechnung
            // 
            this.tsmAbrechnung.Name = "tsmAbrechnung";
            this.tsmAbrechnung.Size = new System.Drawing.Size(85, 20);
            this.tsmAbrechnung.Text = "Abrechnung";
            this.tsmAbrechnung.Click += new System.EventHandler(this.tsmAbrechnung_Click);
            // 
            // frmHaupt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 631);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.mnuHaupt);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnuHaupt;
            this.MinimumSize = new System.Drawing.Size(815, 630);
            this.Name = "frmHaupt";
            this.Text = "Kostenrechner";
            this.Load += new System.EventHandler(this.frmHaupt_Load);
            this.mnuHaupt.ResumeLayout(false);
            this.mnuHaupt.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip mnuHaupt;
        private System.Windows.Forms.ToolStripMenuItem tsmNeu;
        private System.Windows.Forms.ToolStripMenuItem tsmAbrechnung;
    }
}

