﻿namespace Abrechnung
{
    partial class frmAbrechnung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAktivity = new System.Windows.Forms.Label();
            this.pnlAktivity = new System.Windows.Forms.Panel();
            this.txtActivity = new System.Windows.Forms.TextBox();
            this.gbAbrechnung = new System.Windows.Forms.GroupBox();
            this.checkAnteile = new System.Windows.Forms.CheckBox();
            this.lblTeilnahme = new System.Windows.Forms.Label();
            this.lblTeilnehmer = new System.Windows.Forms.Label();
            this.pnlAnteil = new System.Windows.Forms.Panel();
            this.pnlAktivity.SuspendLayout();
            this.gbAbrechnung.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblAktivity
            // 
            this.lblAktivity.AutoSize = true;
            this.lblAktivity.Location = new System.Drawing.Point(3, 13);
            this.lblAktivity.Name = "lblAktivity";
            this.lblAktivity.Size = new System.Drawing.Size(45, 13);
            this.lblAktivity.TabIndex = 0;
            this.lblAktivity.Text = "Aktivität";
            // 
            // pnlAktivity
            // 
            this.pnlAktivity.Controls.Add(this.txtActivity);
            this.pnlAktivity.Controls.Add(this.lblAktivity);
            this.pnlAktivity.Location = new System.Drawing.Point(12, 57);
            this.pnlAktivity.Name = "pnlAktivity";
            this.pnlAktivity.Size = new System.Drawing.Size(200, 61);
            this.pnlAktivity.TabIndex = 1;
            // 
            // txtActivity
            // 
            this.txtActivity.Location = new System.Drawing.Point(84, 10);
            this.txtActivity.Name = "txtActivity";
            this.txtActivity.Size = new System.Drawing.Size(100, 20);
            this.txtActivity.TabIndex = 1;
            // 
            // gbAbrechnung
            // 
            this.gbAbrechnung.Controls.Add(this.checkAnteile);
            this.gbAbrechnung.Controls.Add(this.lblTeilnahme);
            this.gbAbrechnung.Controls.Add(this.lblTeilnehmer);
            this.gbAbrechnung.Controls.Add(this.pnlAnteil);
            this.gbAbrechnung.Location = new System.Drawing.Point(253, 67);
            this.gbAbrechnung.Name = "gbAbrechnung";
            this.gbAbrechnung.Size = new System.Drawing.Size(463, 475);
            this.gbAbrechnung.TabIndex = 2;
            this.gbAbrechnung.TabStop = false;
            this.gbAbrechnung.Text = "Abrechnung";
            // 
            // checkAnteile
            // 
            this.checkAnteile.AutoSize = true;
            this.checkAnteile.Location = new System.Drawing.Point(9, 34);
            this.checkAnteile.Name = "checkAnteile";
            this.checkAnteile.Size = new System.Drawing.Size(170, 17);
            this.checkAnteile.TabIndex = 3;
            this.checkAnteile.Text = "Alle zahlen den gleichen Anteil";
            this.checkAnteile.UseVisualStyleBackColor = true;
            this.checkAnteile.CheckedChanged += new System.EventHandler(this.checkAnteile_CheckedChanged);
            // 
            // lblTeilnahme
            // 
            this.lblTeilnahme.AutoSize = true;
            this.lblTeilnahme.Location = new System.Drawing.Point(106, 93);
            this.lblTeilnahme.Name = "lblTeilnahme";
            this.lblTeilnahme.Size = new System.Drawing.Size(79, 13);
            this.lblTeilnahme.TabIndex = 2;
            this.lblTeilnahme.Text = "Teilgenommen:";
            // 
            // lblTeilnehmer
            // 
            this.lblTeilnehmer.AutoSize = true;
            this.lblTeilnehmer.Location = new System.Drawing.Point(6, 93);
            this.lblTeilnehmer.Name = "lblTeilnehmer";
            this.lblTeilnehmer.Size = new System.Drawing.Size(62, 13);
            this.lblTeilnehmer.TabIndex = 1;
            this.lblTeilnehmer.Text = "Teilnehmer:";
            // 
            // pnlAnteil
            // 
            this.pnlAnteil.Location = new System.Drawing.Point(257, 93);
            this.pnlAnteil.Name = "pnlAnteil";
            this.pnlAnteil.Size = new System.Drawing.Size(200, 376);
            this.pnlAnteil.TabIndex = 0;
            this.pnlAnteil.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAnteil_Paint);
            // 
            // frmAbrechnung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 591);
            this.Controls.Add(this.gbAbrechnung);
            this.Controls.Add(this.pnlAktivity);
            this.Name = "frmAbrechnung";
            this.Text = "Abrechnung";
            this.Load += new System.EventHandler(this.frmAbrechnung_Load);
            this.pnlAktivity.ResumeLayout(false);
            this.pnlAktivity.PerformLayout();
            this.gbAbrechnung.ResumeLayout(false);
            this.gbAbrechnung.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblAktivity;
        private System.Windows.Forms.Panel pnlAktivity;
        private System.Windows.Forms.TextBox txtActivity;
        private System.Windows.Forms.GroupBox gbAbrechnung;
        private System.Windows.Forms.Label lblTeilnehmer;
        private System.Windows.Forms.Panel pnlAnteil;
        private System.Windows.Forms.Label lblTeilnahme;
        private System.Windows.Forms.CheckBox checkAnteile;
    }
}