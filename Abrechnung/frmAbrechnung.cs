﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Abrechnung
{
    public partial class frmAbrechnung : Form
    {
        public frmAnlegen eltern;
        Label[] arrLblNamen;
        CheckBox[] arrCheckBox;
        int posX = 10;
        int posY = 120;
        int posYcb = 115;
        int posXcb = 120;

        public frmAbrechnung()
        {
            InitializeComponent();
            arrLblNamen = new Label[clsGlobal.arrTxtTeilnehmer.Length];
            arrCheckBox = new CheckBox[clsGlobal.arrTxtTeilnehmer.Length];
        }

        private void frmAbrechnung_Load(object sender, EventArgs e)
        {
            int i;
            for (i = 0; i < arrLblNamen.Length; i++)
            {
                arrLblNamen[i] = new Label();
                arrCheckBox[i] = new CheckBox();

                arrLblNamen[i].Width = 80;
                
                arrLblNamen[i].Location = new Point(posX, posY);
                
                arrCheckBox[i].Location = new Point(posXcb, posYcb);
                arrLblNamen[i].Text = clsGlobal.arrTxtTeilnehmer[i].Text;
                posY += arrLblNamen[i].Height + 10;
                posYcb += arrLblNamen[i].Height + 10;
                gbAbrechnung.Controls.Add(arrLblNamen[i]);
                gbAbrechnung.Controls.Add(arrCheckBox[i]);
            }
            pnlAnteil.Visible = false;

        }

        private void pnlAnteil_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void checkAnteile_CheckedChanged(object sender, EventArgs e)
        {
            if(pnlAnteil.Visible == true)
            {
                pnlAnteil.Visible = false;
            }
            else
            {
                pnlAnteil.Visible = true;
            }
        }
    }
}
