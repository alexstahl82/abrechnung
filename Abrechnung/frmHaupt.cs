﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Abrechnung
{
    public partial class frmHaupt : Form
    {
        

        public frmHaupt()
        {
            InitializeComponent();
            tsmNeu.Enabled = false;
            tsmAbrechnung.Enabled = false;


        }
        private void frmHaupt_Load(object sender, EventArgs e)
        {
            String connectionString = "Server=(local);Database=JSdbAbrechnung;Trusted_Connection=True;";


            clsGlobal.conn = new SqlConnection(connectionString);

            try
            {
                clsGlobal.conn.Open();
                tsmNeu.Enabled = true;
                tsmAbrechnung.Enabled = true;

                // MessageBox.Show("Die Datenbankverbindung konnte hergestellt werden.", "Klappt", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            catch (Exception)
            {
                MessageBox.Show("Die Datenbankverbindung konnte nicht hergestellt werden.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {

        }

        private void tsmNeu_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.MdiChildren.Length; i++)
                if (this.MdiChildren[i] is frmAnlegen)
                {
                    this.MdiChildren[i].BringToFront();
                    return;
                }
            frmAnlegen frm = new frmAnlegen();
            // fast alle Eigenschaften kann man auch im Eigenschaftsfenster setzen
            frm.ShowInTaskbar = false;
            frm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            frm.WindowState = FormWindowState.Maximized;
            frm.ControlBox = false;
            frm.MdiParent = this;
            frm.Show();
        }

        private void tsmAbrechnung_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.MdiChildren.Length; i++)
                if (this.MdiChildren[i] is frmAbrechnung)
                {
                    this.MdiChildren[i].BringToFront();
                    return;
                }
            frmAbrechnung frm = new frmAbrechnung();
            // fast alle Eigenschaften kann man auch im Eigenschaftsfenster setzen
            frm.ShowInTaskbar = false;
            frm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            frm.WindowState = FormWindowState.Maximized;
            frm.ControlBox = false;
            frm.MdiParent = this;
            frm.Show();
        }


    }
}
