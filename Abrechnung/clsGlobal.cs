﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Abrechnung
{
    class clsGlobal
    {
        public static Label[] arrTxtTeilnehmer;
        public static double budgetWarnung;
        public static bool budgetMessageShown = false;
        public static SqlConnection conn;
        public static SqlConnectionStringBuilder csb;
    }   
}
