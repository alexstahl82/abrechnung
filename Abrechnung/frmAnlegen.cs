﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Abrechnung
{
    public partial class frmAnlegen : Form
    {

        frmAbrechnung abrechnung;
        frmHaupt haupt;

        public frmAnlegen()
        {
            InitializeComponent();
        }

        private void frmAnlegen_Load(object sender, EventArgs e)
        {
            //cbTeilnehmer.SelectedIndex = 1;
            String SQLString;

            System.Data.SqlClient.SqlDataAdapter da;
            DataTable dt;
            DataRow dr;


            dt = new DataTable();



            SQLString = "SELECT * FROM tblTeilnehmer";

            da = new SqlDataAdapter(SQLString, clsGlobal.conn);

            da.Fill(dt);
            dr = dt.NewRow();
   
            dgvTeilnehmer.DataSource = dt;
            dgvTeilnehmer.Columns["ID_Teilnehmer"].Visible = false;

        }



        private void cbTeilnehmer_SelectedIndexChanged(object sender, EventArgs e)
        {
            gbTeilnehmer.Controls.Clear();
            clsGlobal.arrTxtTeilnehmer = new Label[Convert.ToInt32(cbTeilnehmer.Text)];
            int posX = 10;
            int posY = 30;

            for (int i = 0; i < clsGlobal.arrTxtTeilnehmer.Length; i++)
            {
                clsGlobal.arrTxtTeilnehmer[i] = new Label();
                clsGlobal.arrTxtTeilnehmer[i].Location = new System.Drawing.Point(posX, posY);
                posY += clsGlobal.arrTxtTeilnehmer[i].Height * 2;
                gbTeilnehmer.Controls.Add(clsGlobal.arrTxtTeilnehmer[i]);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvTeilnehmer_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}
