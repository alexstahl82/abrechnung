﻿namespace Abrechnung
{
    partial class frmAnlegen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbTeilnehmer = new System.Windows.Forms.ComboBox();
            this.gbTeilnehmer = new System.Windows.Forms.GroupBox();
            this.lblBudget = new System.Windows.Forms.Label();
            this.checkBudget = new System.Windows.Forms.CheckBox();
            this.gbBudget = new System.Windows.Forms.GroupBox();
            this.txtBudget = new System.Windows.Forms.TextBox();
            this.lblAnzTeilnehmer = new System.Windows.Forms.Label();
            this.dgvTeilnehmer = new System.Windows.Forms.DataGridView();
            this.gbBudget.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeilnehmer)).BeginInit();
            this.SuspendLayout();
            // 
            // cbTeilnehmer
            // 
            this.cbTeilnehmer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTeilnehmer.FormattingEnabled = true;
            this.cbTeilnehmer.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cbTeilnehmer.Location = new System.Drawing.Point(219, 53);
            this.cbTeilnehmer.Name = "cbTeilnehmer";
            this.cbTeilnehmer.Size = new System.Drawing.Size(60, 21);
            this.cbTeilnehmer.TabIndex = 4;
            this.cbTeilnehmer.SelectedIndexChanged += new System.EventHandler(this.cbTeilnehmer_SelectedIndexChanged);
            // 
            // gbTeilnehmer
            // 
            this.gbTeilnehmer.Location = new System.Drawing.Point(332, 101);
            this.gbTeilnehmer.Name = "gbTeilnehmer";
            this.gbTeilnehmer.Size = new System.Drawing.Size(195, 434);
            this.gbTeilnehmer.TabIndex = 5;
            this.gbTeilnehmer.TabStop = false;
            this.gbTeilnehmer.Text = "Teilnehmer";
            // 
            // lblBudget
            // 
            this.lblBudget.AutoSize = true;
            this.lblBudget.Location = new System.Drawing.Point(14, 80);
            this.lblBudget.Name = "lblBudget";
            this.lblBudget.Size = new System.Drawing.Size(69, 13);
            this.lblBudget.TabIndex = 6;
            this.lblBudget.Text = "In Höhe von:";
            // 
            // checkBudget
            // 
            this.checkBudget.AutoSize = true;
            this.checkBudget.Location = new System.Drawing.Point(17, 47);
            this.checkBudget.Name = "checkBudget";
            this.checkBudget.Size = new System.Drawing.Size(153, 17);
            this.checkBudget.TabIndex = 7;
            this.checkBudget.Text = "Ich habe ein festes Budget";
            this.checkBudget.UseVisualStyleBackColor = true;
            // 
            // gbBudget
            // 
            this.gbBudget.Controls.Add(this.txtBudget);
            this.gbBudget.Controls.Add(this.checkBudget);
            this.gbBudget.Controls.Add(this.lblBudget);
            this.gbBudget.Location = new System.Drawing.Point(545, 53);
            this.gbBudget.Name = "gbBudget";
            this.gbBudget.Size = new System.Drawing.Size(200, 148);
            this.gbBudget.TabIndex = 8;
            this.gbBudget.TabStop = false;
            this.gbBudget.Text = "Budget";
            // 
            // txtBudget
            // 
            this.txtBudget.Location = new System.Drawing.Point(17, 110);
            this.txtBudget.Name = "txtBudget";
            this.txtBudget.Size = new System.Drawing.Size(100, 20);
            this.txtBudget.TabIndex = 8;
            // 
            // lblAnzTeilnehmer
            // 
            this.lblAnzTeilnehmer.AutoSize = true;
            this.lblAnzTeilnehmer.Location = new System.Drawing.Point(12, 56);
            this.lblAnzTeilnehmer.Name = "lblAnzTeilnehmer";
            this.lblAnzTeilnehmer.Size = new System.Drawing.Size(183, 13);
            this.lblAnzTeilnehmer.TabIndex = 9;
            this.lblAnzTeilnehmer.Text = "Wie viele Leute werden Teilnehmen?";
            // 
            // dgvTeilnehmer
            // 
            this.dgvTeilnehmer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTeilnehmer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTeilnehmer.Location = new System.Drawing.Point(12, 101);
            this.dgvTeilnehmer.Name = "dgvTeilnehmer";
            this.dgvTeilnehmer.Size = new System.Drawing.Size(249, 434);
            this.dgvTeilnehmer.TabIndex = 10;
            this.dgvTeilnehmer.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTeilnehmer_CellClick);
            // 
            // frmAnlegen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 631);
            this.Controls.Add(this.dgvTeilnehmer);
            this.Controls.Add(this.lblAnzTeilnehmer);
            this.Controls.Add(this.cbTeilnehmer);
            this.Controls.Add(this.gbBudget);
            this.Controls.Add(this.gbTeilnehmer);
            this.Name = "frmAnlegen";
            this.Text = "Anlegen";
            this.Load += new System.EventHandler(this.frmAnlegen_Load);
            this.gbBudget.ResumeLayout(false);
            this.gbBudget.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTeilnehmer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbTeilnehmer;
        private System.Windows.Forms.GroupBox gbTeilnehmer;
        private System.Windows.Forms.Label lblBudget;
        private System.Windows.Forms.CheckBox checkBudget;
        private System.Windows.Forms.GroupBox gbBudget;
        private System.Windows.Forms.Label lblAnzTeilnehmer;
        private System.Windows.Forms.TextBox txtBudget;
        private System.Windows.Forms.DataGridView dgvTeilnehmer;
    }
}